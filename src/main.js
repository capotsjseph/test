import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import ioPlugin from './plugins/io.js'
import lodashPlugin from 'lodash'
import router from '@/router'
import store from '@/store/index'

Vue.config.productionTip = false
Vue.use(ioPlugin)
Vue.use(lodashPlugin)

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')

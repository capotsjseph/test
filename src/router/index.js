import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'


Vue.use(Router)

export const router = new Router ({

    routes: [
        {
            path: '/login',
            name:'Login',
            component: Login,
            meta: {public: true}
        },
        {
            path: '/dashboard',
            name:'Dashboard',
            component: Dashboard,
            meta: {public: true}
        }
    ]
})

export default router
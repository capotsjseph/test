export default {
    AUTH_REQUEST ({state}, {username,password}) {
        return state.socketio.socket.post('/user/login', {username: username, password: password}, function (response){
            if (response.err === null) {
                console.log('hello')
                location.href = '/#/dashboard'
            }
        })
    },
}